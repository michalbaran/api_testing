import requests


class GoRESTHandler:

    base_url = "https://gorest.co.in/public/v2"
    users_endpoint = "/users"

    headers = {
        "Authorization": "Bearer 05736be32f1effc7eb1fab8aed0abb2e6375ccca2fa2a3760053e5046132f1f4"
    }


    def create_user(self, user_data):
       response = requests.post(self.base_url+self.users_endpoint, json=user_data, headers=self.headers)
       assert response.status_code == 201
       return response

    def get_user_by_id(self, user_id):
        response = requests.get(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 200
        return response

    def update_user(self, user_data_updated, user_id):
        response = requests.patch(f"{self.base_url}{self.users_endpoint}/{user_id}", json=user_data_updated, headers=self.headers)
        assert response.status_code == 200
        return response

    def delete_user(self, user_id):
        response = requests.delete(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 204
        return response





