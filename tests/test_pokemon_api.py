import requests
from utils.pokeapi_handler import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


def test_default_list_of_pokemons():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    response_body = response.json()

    assert response.status_code == 200
    assert len(response_body["results"]) > 0

    assert response_body["count"] == 1279

    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    response_size_bytes = len(response.content)
    assert response_size_bytes < 100 * 1000


def test_pagination():
    params = {
        "limit": 10,
        "offset": 20
    }

    response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
    body = response.json()

    assert len(body["results"]) == params["limit"]

    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + 1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"


def test_shapes():
    body = pokeapi_handler.get_shapes_of_pokemons().json()

    assert body["count"] == len(body["results"])

    pokemon_shape = body["results"][2]["name"]
    body = pokeapi_handler.get_shapes_of_pokemons(pokemon_shape).json()
    assert body["id"] == 3
